﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2;
        }

        public int Minus(int value1, int value2)
        {
            return value1 - value2;
        }

        public float Times(float value1, float value2)
        {
            return value1 * value2;
        }

        public Double DividedBy(float value1, float value2)
        {
            if (value2 == 0)
            {
                return 0;
            }
            else
            {

                return value1 / value2;
            }
        }


    }
}
