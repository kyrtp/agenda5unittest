﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }



 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Minus()
        {
            int value1 = 3;
            int value2 = 4;
            int expected = -1;
            int actual = SystemUnderTest.Minus(value1, value2);
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Times()
        {
            float value1 = 4;
            float value2 = 10;
            float expected = 40;
            float actual = SystemUnderTest.Times(value1, value2);
            Assert.AreEqual<float>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void DividedBy()
        {
            float value1 = 9;
            float value2 = 2;
            Double expected = 4.5;
            Double actual = SystemUnderTest.DividedBy(value1, value2);
            Assert.AreEqual<Double>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void DividedByZero()
        {
            float value1 = 2;
            Double expected = 0;
            Double actual = SystemUnderTest.DividedBy(value1, 0);
            Assert.AreEqual<Double>(expected, actual, "Error, no se puede dividir entre cero");
        }
    }
}
